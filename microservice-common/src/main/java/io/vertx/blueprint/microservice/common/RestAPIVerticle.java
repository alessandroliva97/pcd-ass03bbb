package io.vertx.blueprint.microservice.common;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;
import java.util.HashSet;
import java.util.Set;

public abstract class RestAPIVerticle extends BaseMicroserviceVerticle {

	/**
	 * Create http server for the REST service.
	 *
	 * @param router router instance
	 * @param host   http host
	 * @param port   http port
	 * @return async result of the procedure
	 */
	protected Future<Void> createHttpServer(Router router, String host, int port) {
		Future<HttpServer> httpServerFuture = Future.future();
		vertx.createHttpServer().requestHandler(router::accept).listen(port, host, httpServerFuture.completer());
		return httpServerFuture.map(r -> null);
	}

	/**
	 * Enable CORS support.
	 *
	 * @param router router instance
	 */
	protected void enableCorsSupport(Router router) {
		Set<String> allowHeaders = new HashSet<>();
		allowHeaders.add("x-requested-with");
		allowHeaders.add("Access-Control-Allow-Origin");
		allowHeaders.add("origin");
		allowHeaders.add("Content-Type");
		allowHeaders.add("accept");
		allowHeaders.add("Authorization");
		allowHeaders.add("Access-Control-Allow-Credentials");
		Set<HttpMethod> allowMethods = new HashSet<>();
		allowMethods.add(HttpMethod.GET);
		allowMethods.add(HttpMethod.PUT);
		allowMethods.add(HttpMethod.OPTIONS);
		allowMethods.add(HttpMethod.POST);
		allowMethods.add(HttpMethod.DELETE);
		allowMethods.add(HttpMethod.PATCH);

		router.route().handler(CorsHandler.create("*").allowedHeaders(allowHeaders).allowedMethods(allowMethods)
				.allowCredentials(true));
	}

	// helper result handler within a request context

	/**
	 * This method generates handler for async methods in REST APIs.
	 */
	protected <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Handler<T> handler) {
		return res -> {
			if (res.succeeded()) {
				handler.handle(res.result());
			} else {
				internalError(context, res.cause());
				res.cause().printStackTrace();
			}
		};
	}

	/**
	 * This method generates handler for async methods in REST APIs. The result
	 * requires non-empty. If empty, return <em>404 Not Found</em> status. The
	 * content type is JSON.
	 *
	 * @param context routing context instance
	 * @param <T>     result type
	 * @return generated handler
	 */
	protected <T> Handler<AsyncResult<T>> resultHandlerNonEmpty(RoutingContext context) {
		return ar -> {
			if (ar.succeeded()) {
				T res = ar.result();
				if (res == null) {
					notFound(context);
				} else {
					context.response().putHeader("content-type", "application/json").end(res.toString());
				}
			} else {
				internalError(context, ar.cause());
				ar.cause().printStackTrace();
			}
		};
	}

	// helper method dealing with failure

	protected void badRequest(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(400).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	protected void notFound(RoutingContext context) {
		context.response().setStatusCode(404).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("message", "not_found").encodePrettily());
	}

	protected void internalError(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(500).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	protected void notImplemented(RoutingContext context) {
		context.response().setStatusCode(501).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("message", "not_implemented").encodePrettily());
	}

	protected void badGateway(Throwable ex, RoutingContext context) {
		ex.printStackTrace();
		context.response().setStatusCode(502).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("message", ex.getMessage()).encodePrettily());
	}

	protected void serviceUnavailable(RoutingContext context) {
		context.fail(503);
	}

	protected void serviceUnavailable(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(503).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	protected void serviceUnavailable(RoutingContext context, String cause) {
		context.response().setStatusCode(503).putHeader("content-type", "application/json")
				.putHeader("Access-Control-Allow-Origin", "*")
				.end(new JsonObject().put("error", cause).encodePrettily());
	}
}
