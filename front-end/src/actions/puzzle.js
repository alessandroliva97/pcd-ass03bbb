import axios from 'axios';

var CONFIG = require('../config.json');
const address = CONFIG.host + '/api/puzzle';

export async function getRowsNum() {
  try {
    const result = await axios.get(address + '/rows');
    return result.data.rows;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function getColumnsNum() {
  try {
    const result = await axios.get(address + '/columns');
    return result.data.columns;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function getImage() {
  try {
    const result = await axios.get(address + '/image');
    return result.data.image;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function getTiles() {
  try {
    const result = await axios.get(address + '/tiles');
    return result.data.tiles;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function selectTile(position, id) {
  try {
    const result = await axios.post(address + `/select/${position}/${id}`);
    return result.data;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function deselectTile(id) {
  try {
    const result = await axios.post(address + `/deselect/${id}`);
    return result.data;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function getLastColor(position) {
  try {
    const result = await axios.get(address + `/lastcolor/${position}`);
    return result.data.color;
  } catch(err) {
    console.log(err.response.color);
  }
};
