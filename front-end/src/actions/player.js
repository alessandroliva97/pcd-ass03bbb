import axios from 'axios';

var CONFIG = require('../config.json');
const address = CONFIG.host + '/api/player';

export async function getPlayerId() {
  try {
    const result = await axios.get(address + '/identifier');
    return result.data.identifier;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function getPlayerColor(id) {
  try {
    const result = await axios.get(address + `/color/${id}`);
    return result.data.color;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function addPlayer(id) {
  try {
    const result = await axios.put(address + `/player/${id}`);
    return result.data.message;
  } catch(err) {
    console.log(err.response.statusText);
  }
};

export async function deletePlayer(id) {
  try {
    const result = await axios.delete(address + `/player/${id}`);
    return result.data.message;
  } catch(err) {
    console.log(err.response.statusText);
  }
};
