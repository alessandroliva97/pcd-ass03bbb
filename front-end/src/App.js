import React, { Component, Fragment } from "react";

import { getPlayerId, getPlayerColor, addPlayer, deletePlayer } from './actions/player';
import { getRowsNum, getColumnsNum, getImage, getTiles, selectTile, deselectTile, getLastColor } from './actions/puzzle';
import Tile from './Tile.js';
import EventBus from 'vertx3-eventbus-client'

var CONFIG = require('./config.json');
const eb = new EventBus(CONFIG.host + "/eventbus");

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id : '',
      color : '',
      rows : '',
      columns : '',
      img : '',
      tiles : [],
      positionColors: []
    }
  }

  componentDidMount() {
    eb.onopen = this.onOpen(this);

    getPlayerId().then(res => {
      this.setState({ id: res }, () => {
        this.state.id && addPlayer(this.state.id).then(res => {
          this.state.id && getPlayerColor(this.state.id).then(r => {
            this.setState({ color: r })
          });
        });
      })
    });
    getRowsNum().then(res => {
      this.setState({ rows : res });

      getColumnsNum().then(res => {
        this.setState({ columns : res }, () => {
          if (this.state.rows && this.state.columns) {
            for (let i = 0; i < this.state.rows * this.state.columns; i++) {
              getLastColor(i).then(res => {
                this.state.positionColors.push({[i]: res})
              });
            }
          }
        });
      });
    });
    getImage().then(res => this.setState({ img : res }));
    getTiles().then(res => {
      this.setState({ tiles : res })
    });

    window.addEventListener("beforeunload", this.beforeunload.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener("beforeunload", this.beforeunload.bind(this));
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.tiles !== this.state.tiles && Array.isArray(this.state.tiles) && this.state.tiles.length > 0 && this.state.tiles.every(t => t.originalPosition === t.currentPosition)) {
      alert("Puzzle completed");
    }
  }

  onOpen(component) {
    return function() {
      // set a handler to receive a message
      eb.registerHandler('update', function(error, message) {
        getTiles().then(res => {
          component.setState({ tiles : res })
        });
        for (let i = 0; i < component.state.rows*component.state.columns; i++) {
          getLastColor(i).then(res => {
            const { positionColors } = component.state;
            positionColors[i] = res;
            component.setState({ positionColors });
          });
        }
      });
    }
  }

  beforeunload(e) {
    this.state.id && deselectTile(this.state.id).then(res => deletePlayer(this.state.id));
  }

  render() {
    return (
      <Fragment>
      <h1>Puzzle multiplayer</h1>
      {
        this.state.id && this.state.color && <h2 style={{color: this.state.color}}>{'Welcome, player ' + this.state.id}</h2>
      }
      <div className='board'>
      {
        this.state.tiles && this.state.rows && this.state.id && this.state.tiles.map((tile, key) => (
          <Tile key={key}
          originalPosition={tile.originalPosition}
          currentPosition={tile.currentPosition}
          image={this.state.img}
          rows={this.state.rows}
          color={this.state.positionColors[tile.currentPosition]}
          onClick={() => selectTile(tile.currentPosition, this.state.id)} />
        ))
      }
      </div>
      </Fragment>
    );
  }
}

export default App;
