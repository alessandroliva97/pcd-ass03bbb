import React from "react";

export default function Tile({originalPosition, currentPosition, image, rows, color, onClick}) {
	const currentColumn = currentPosition % rows;
  const currentRow = Math.trunc(currentPosition / rows);
    const top = currentColumn*100 + 5;
    const left = currentRow*100 + 5;
    const bgLeft = (Math.trunc(originalPosition/rows) )*100 + 5;
    const bgTop = Math.floor(originalPosition%rows)*100 + 5;

    return <div
      className='tile'
      onClick={onClick}
      style={{top, left, backgroundPosition: `-${bgLeft}px -${bgTop}px`, backgroundImage: `url('${image}')`, borderColor: `${color}`}}
    />;
}
