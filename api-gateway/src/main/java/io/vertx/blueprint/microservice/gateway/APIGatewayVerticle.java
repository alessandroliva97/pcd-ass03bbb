package io.vertx.blueprint.microservice.gateway;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

import java.util.List;
import java.util.Optional;

public class APIGatewayVerticle extends RestAPIVerticle {

	private static final int DEFAULT_PORT = 8787;

	private static final String DEFAULT_CLIENT_HOST = "192.168.99.100";

	private static final Logger logger = LoggerFactory.getLogger(APIGatewayVerticle.class);

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();

		// get HTTP host and port from configuration, or use default value
		String host = config().getString("api.gateway.http.address", "0.0.0.0");
		int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT);

		Router router = Router.router(vertx);

		SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
		BridgeOptions options = new BridgeOptions().addOutboundPermitted(new PermittedOptions().setAddress("update"));
		sockJSHandler.bridge(options);

		// body handler
		router.route().handler(BodyHandler.create());

		// version handler
		router.get("/api/v").handler(this::apiVersion);

		router.route("/eventbus/*").handler(sockJSHandler);

		// api dispatcher
		router.route("/api/*").handler(this::dispatchRequests);

		// static content
		router.route("/*").handler(StaticHandler.create());

		// create http server
		vertx.createHttpServer().requestHandler(router::accept).listen(port, host, ar -> {
			if (ar.succeeded()) {
				publishApiGateway(host, port);
				future.complete();
				logger.info("API Gateway is running on port " + port);
				// publish log
				publishGatewayLog("api_gateway_init_success:" + port);
			} else {
				future.fail(ar.cause());
			}
		});
	}

	private void dispatchRequests(RoutingContext context) {
		int initialOffset = 5; // length of `/api/`
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			getAllEndpoints().setHandler(ar -> {
				if (ar.succeeded()) {
					List<Record> recordList = ar.result();
					// get relative path and retrieve prefix to dispatch client
					String path = context.request().uri();

					if (path.length() <= initialOffset) {
						notFound(context);
						future.complete();
						return;
					}
					String prefix = (path.substring(initialOffset).split("/"))[0];
					// generate new relative path
					String newPath = path.substring(initialOffset + prefix.length());
					// get one relevant HTTP client, may not exist
					Optional<Record> client = recordList.stream()
							.filter(record -> record.getMetadata().getString("api.name") != null)
							.filter(record -> record.getMetadata().getString("api.name").equals(prefix)).findAny();

					if (client.isPresent()) {
						doDispatch(context,
								(client.get().getMetadata().getString("api.name").equals("puzzle") ? 8082 : 8081),
								DEFAULT_CLIENT_HOST, newPath, discovery.getReference(client.get()).get(), future);
					} else {
						notFound(context);
						future.complete();
					}
				} else {
					future.fail(ar.cause());
				}
			});
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
			}
		});
	}

	/**
	 * Dispatch the request to the downstream REST layers.
	 *
	 * @param context routing context instance
	 * @param port    client port
	 * @param host    client host
	 * @param path    relative path
	 * @param client  relevant HTTP client
	 */
	private void doDispatch(RoutingContext context, int port, String host, String path, HttpClient client,
			Future<Object> cbFuture) {
		HttpClientRequest toReq = client.request(context.request().method(), port, host, path, response -> {

			response.bodyHandler(body -> {
				if (response.statusCode() >= 500) { // api endpoint server error, circuit breaker should fail
					cbFuture.fail(response.statusCode() + ": " + body.toString());
				} else {
					HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
					response.headers().forEach(header -> {
						toRsp.putHeader(header.getKey(), header.getValue());
					});
					// send response
					toRsp.end(body);
					cbFuture.complete();
				}
				ServiceDiscovery.releaseServiceObject(discovery, client);
			});
		});
		// set headers
		context.request().headers().forEach(header -> {
			toReq.putHeader(header.getKey(), header.getValue());
		});
		if (context.user() != null) {
			toReq.putHeader("user-principal", context.user().principal().encode());
		}
		// send request
		if (context.getBody() == null) {
			toReq.end();
		} else {
			toReq.end(context.getBody());
		}
	}

	private void apiVersion(RoutingContext context) {
		context.response().end(new JsonObject().put("version", "v1").encodePrettily());
	}

	/**
	 * Get all REST endpoints from the service discovery infrastructure.
	 *
	 * @return async result
	 */
	private Future<List<Record>> getAllEndpoints() {
		Future<List<Record>> future = Future.future();
		discovery.getRecords(record -> record.getType().equals(HttpEndpoint.TYPE), future.completer());
		return future;
	}

	// log methods

	private void publishGatewayLog(String info) {
		JsonObject message = new JsonObject().put("info", info).put("time", System.currentTimeMillis());
		publishLogEvent("gateway", message);
	}

}
