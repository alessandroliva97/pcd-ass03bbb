/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module vertx-blueprint-player-js/player_service */
var utils = require('vertx-js/util/utils');

var io = Packages.io;
var JsonObject = io.vertx.core.json.JsonObject;
var JPlayerService = Java.type('io.vertx.blueprint.microservice.player.PlayerService');

/**
 @class
*/
var PlayerService = function(j_val) {

  var j_playerService = j_val;
  var that = this;

  /**

   @public
   @param id {string} 
   @param resultHandler {function} 
   @return {PlayerService}
   */
  this.registerPlayer = function(id, resultHandler) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_playerService["registerPlayer(java.lang.String,io.vertx.core.Handler)"](id, function(ar) {
      if (ar.succeeded()) {
        resultHandler(null, null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param id {string} 
   @param resultHandler {function} 
   @return {PlayerService}
   */
  this.deletePlayer = function(id, resultHandler) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_playerService["deletePlayer(java.lang.String,io.vertx.core.Handler)"](id, function(ar) {
      if (ar.succeeded()) {
        resultHandler(null, null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param id {string} 
   @param resultHandlerNonEmpty {function} 
   @return {PlayerService}
   */
  this.retrieveColor = function(id, resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_playerService["retrieveColor(java.lang.String,io.vertx.core.Handler)"](id, function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param resultHandlerNonEmpty {function} 
   @return {PlayerService}
   */
  this.retrieveNextIdentifier = function(resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_playerService["retrieveNextIdentifier(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param handler {function} 
   @return {PlayerService}
   */
  this.retrieveClients = function(handler) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_playerService["retrieveClients(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        handler(utils.convReturnJson(ar.result()), null);
      } else {
        handler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  // A reference to the underlying Java delegate
  // NOTE! This is an internal API and must not be used in user code.
  // If you rely on this property your code is likely to break if we change it / remove it without warning.
  this._jdel = j_playerService;
};

PlayerService._jclass = utils.getJavaClass("io.vertx.blueprint.microservice.player.PlayerService");
PlayerService._jtype = {
  accept: function(obj) {
    return PlayerService._jclass.isInstance(obj._jdel);
  },
  wrap: function(jdel) {
    var obj = Object.create(PlayerService.prototype, {});
    PlayerService.apply(obj, arguments);
    return obj;
  },
  unwrap: function(obj) {
    return obj._jdel;
  }
};
PlayerService._create = function(jdel) {
  var obj = Object.create(PlayerService.prototype, {});
  PlayerService.apply(obj, arguments);
  return obj;
}
module.exports = PlayerService;