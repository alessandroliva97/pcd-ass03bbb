/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package io.vertx.blueprint.microservice.player.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(io.vertx.blueprint.microservice.player.PlayerService.class)
public class PlayerService {

  public static final io.vertx.lang.rxjava.TypeArg<PlayerService> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new PlayerService((io.vertx.blueprint.microservice.player.PlayerService) obj),
    PlayerService::getDelegate
  );

  private final io.vertx.blueprint.microservice.player.PlayerService delegate;
  
  public PlayerService(io.vertx.blueprint.microservice.player.PlayerService delegate) {
    this.delegate = delegate;
  }

  public io.vertx.blueprint.microservice.player.PlayerService getDelegate() {
    return delegate;
  }

  public PlayerService registerPlayer(String id, Handler<AsyncResult<Void>> resultHandler) { 
    delegate.registerPlayer(id, resultHandler);
    return this;
  }

  public Single<Void> rxRegisterPlayer(String id) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      registerPlayer(id, fut);
    }));
  }

  public PlayerService deletePlayer(String id, Handler<AsyncResult<Void>> resultHandler) { 
    delegate.deletePlayer(id, resultHandler);
    return this;
  }

  public Single<Void> rxDeletePlayer(String id) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      deletePlayer(id, fut);
    }));
  }

  public PlayerService retrieveColor(String id, Handler<AsyncResult<String>> resultHandlerNonEmpty) { 
    delegate.retrieveColor(id, resultHandlerNonEmpty);
    return this;
  }

  public Single<String> rxRetrieveColor(String id) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveColor(id, fut);
    }));
  }

  public PlayerService retrieveNextIdentifier(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) { 
    delegate.retrieveNextIdentifier(resultHandlerNonEmpty);
    return this;
  }

  public Single<Integer> rxRetrieveNextIdentifier() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveNextIdentifier(fut);
    }));
  }

  public PlayerService retrieveClients(Handler<AsyncResult<JsonArray>> handler) { 
    delegate.retrieveClients(handler);
    return this;
  }

  public Single<JsonArray> rxRetrieveClients() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveClients(fut);
    }));
  }


  public static PlayerService newInstance(io.vertx.blueprint.microservice.player.PlayerService arg) {
    return arg != null ? new PlayerService(arg) : null;
  }
}
