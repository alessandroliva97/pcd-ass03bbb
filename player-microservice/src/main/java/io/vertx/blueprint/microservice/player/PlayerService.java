package io.vertx.blueprint.microservice.player;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;

import java.awt.Color;
import java.util.List;
import java.util.Map;

@VertxGen
@ProxyGen
public interface PlayerService {

	/**
	 * The name of the event bus service.
	 */
	String SERVICE_NAME = "player-eb-service";

	/**
	 * The address on which the service is published.
	 */
	String SERVICE_ADDRESS = "service.user.player";

	@Fluent
	PlayerService registerPlayer(String id, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	PlayerService deletePlayer(String id, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	PlayerService retrieveColor(String id, Handler<AsyncResult<String>> resultHandlerNonEmpty);

	@Fluent
	PlayerService retrieveNextIdentifier(Handler<AsyncResult<Integer>> resultHandlerNonEmpty);

	@Fluent
	PlayerService retrieveClients(Handler<AsyncResult<JsonArray>> handler);

}
