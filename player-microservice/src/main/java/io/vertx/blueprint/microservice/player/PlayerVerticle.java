package io.vertx.blueprint.microservice.player;

import static io.vertx.blueprint.microservice.player.PlayerService.SERVICE_ADDRESS;
import static io.vertx.blueprint.microservice.player.PlayerService.SERVICE_NAME;
import io.vertx.blueprint.microservice.common.BaseMicroserviceVerticle;
import io.vertx.blueprint.microservice.player.api.RestPlayerAPIVerticle;
import io.vertx.blueprint.microservice.player.impl.PlayerServiceImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ProxyHelper;

public class PlayerVerticle extends BaseMicroserviceVerticle {

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();

		// create the service instance
		PlayerService playerService = new PlayerServiceImpl();
		// register the service proxy on event bus
		ProxyHelper.registerService(PlayerService.class, vertx, playerService, SERVICE_ADDRESS);
		// publish the service in the discovery infrastructure
		publishEventBusService(PlayerService.SERVICE_NAME, SERVICE_ADDRESS, PlayerService.class)
				.compose(servicePublished -> deployRestService(playerService)).setHandler(future.completer());
	}

	private Future<Void> deployRestService(PlayerService service) {
		Future<String> future = Future.future();
		vertx.deployVerticle(new RestPlayerAPIVerticle(service), new DeploymentOptions().setConfig(config()),
				future.completer());
		return future.map(r -> null);
	}

}
