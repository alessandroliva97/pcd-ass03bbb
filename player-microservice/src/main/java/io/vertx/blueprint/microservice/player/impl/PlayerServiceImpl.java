package io.vertx.blueprint.microservice.player.impl;

import io.vertx.blueprint.microservice.player.PlayerService;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;

import java.awt.Color;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class PlayerServiceImpl implements PlayerService {

	private static final SecureRandom RAND = new SecureRandom();

	private final Map<Integer, CustomColor> clients = new HashMap<>();
	private int lastGenId = 0;

	@Override
	public PlayerService registerPlayer(String id, Handler<AsyncResult<Void>> resultHandler) {
		clients.put(Integer.parseInt(id), randomColor());
		resultHandler.handle(Future.succeededFuture());
		return this;
	}

	@Override
	public PlayerService deletePlayer(String id, Handler<AsyncResult<Void>> resultHandler) {
		clients.remove(Integer.parseInt(id));
		resultHandler.handle(Future.succeededFuture());
		return this;
	}

	@Override
	public PlayerService retrieveColor(String id, Handler<AsyncResult<String>> resultHandlerNonEmpty) {
		final Color color = clients.get(Integer.parseInt(id)).getColor();
		resultHandlerNonEmpty.handle(Future
				.succeededFuture(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue())));
		return this;
	}

	@Override
	public PlayerService retrieveNextIdentifier(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) {
		final int id = lastGenId++;
		resultHandlerNonEmpty.handle(Future.succeededFuture(id));
		return this;
	}

	@Override
	public PlayerService retrieveClients(Handler<AsyncResult<JsonArray>> resultHandler) {
		final JsonArray arr = new JsonArray();
		clients.forEach((k, v) -> {
			final JsonObject obj = new JsonObject();
			final Color color = v.getColor();
			obj.put("tile", k);
			obj.put("lastColor", String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
			arr.add(obj);
		});
		resultHandler.handle(Future.succeededFuture(arr));
		return this;
	}

	private static CustomColor randomColor() {
		final int x = RAND.nextInt(CustomColor.class.getEnumConstants().length);
		return CustomColor.class.getEnumConstants()[x];
	}

}
