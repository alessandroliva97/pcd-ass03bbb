package io.vertx.blueprint.microservice.player.impl;

import java.awt.Color;

public enum CustomColor {

	WHITE(new Color(0xffffff, false)), LIGHT_GRAY(new Color(0xc0c0c0, false)), DARK_GRAY(new Color(0x404040, false)),
	BLACK(new Color(0x000000, false)), RED(new Color(0xff0000, false)), PINK(new Color(0xffafaf, false)),
	ORANGE(new Color(0xffc800, false)), YELLOW(new Color(0xffff00, false)), GREEN(new Color(0x00ff00, false)),
	MAGENTA(new Color(0xff00ff, false)), CYAN(new Color(0x00ffff, false)), BLUE(new Color(0x0000ff, false));

	private final Color color;

	CustomColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

}