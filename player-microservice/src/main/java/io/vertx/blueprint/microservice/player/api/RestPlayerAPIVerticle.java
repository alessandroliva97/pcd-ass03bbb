package io.vertx.blueprint.microservice.player.api;

import java.util.Set;
import java.util.HashSet;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.blueprint.microservice.player.PlayerService;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;

public class RestPlayerAPIVerticle extends RestAPIVerticle {

	private static final String SERVICE_NAME = "player-rest-api";

	private final PlayerService playerService;

	private static final String API_PLAYER = "/player/:id";
	private static final String API_COLOR = "/color/:id";
	private static final String API_IDENTIFIER = "/identifier";

	public RestPlayerAPIVerticle(PlayerService playerService) {
		this.playerService = playerService;
	}

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		final Router router = Router.router(vertx);
		// CORS support
		enableCorsSupport(router);
		// body handler
		router.route().handler(BodyHandler.create());
		// api route handler
		router.put(API_PLAYER).handler(this::apiAddPlayer);
		router.delete(API_PLAYER).handler(this::apiDeletePlayer);
		router.get(API_COLOR).handler(this::apiRetrieveColor);
		router.get(API_IDENTIFIER).handler(this::apiGetIdentifier);

		String host = config().getString("player.http.address", "0.0.0.0");
		int port = config().getInteger("player.http.port", 8081);

		// create HTTP server and publish REST service
		createHttpServer(router, host, port).compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port))
				.setHandler(future.completer());
	}

	private void apiAddPlayer(RoutingContext context) {
		try {
			String id = context.request().getParam("id");
			playerService.registerPlayer(id, resultHandler(context, r -> {
				String result = new JsonObject().put("message", "player added").put("id", id).encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*").end(result);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}

	private void apiDeletePlayer(RoutingContext context) {
		try {
			String id = context.request().getParam("id");
			playerService.deletePlayer(id, resultHandler(context, r -> {
				String result = new JsonObject().put("message", "player removed").put("id", id).encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*").end(result);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}

	private void apiRetrieveColor(RoutingContext context) {
		try {
			String id = context.request().getParam("id");
			playerService.retrieveColor(id, resultHandler(context, r -> {
				String result = new JsonObject().put("color", r).encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*").end(result);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}

	private void apiGetIdentifier(RoutingContext context) {
		playerService.retrieveNextIdentifier(resultHandler(context, r -> {
			String result = new JsonObject().put("identifier", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*").end(result);
		}));
	}

	private void apiRetrieveClients(RoutingContext context) {
		playerService.retrieveClients(resultHandler(context, r -> {
			String result = new JsonObject().put("clients", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json").end(result);
		}));
	}

}
