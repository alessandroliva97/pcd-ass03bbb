/**
 * Indicates that this module contains classes that need to be generated /
 * processed.
 */
@ModuleGen(name = "vertx-blueprint-player", groupPackage = "io.vertx.blueprint.microservice.player")
package io.vertx.blueprint.microservice.player;

import io.vertx.codegen.annotations.ModuleGen;