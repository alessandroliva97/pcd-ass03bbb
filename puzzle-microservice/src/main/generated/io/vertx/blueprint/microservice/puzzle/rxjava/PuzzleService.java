/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package io.vertx.blueprint.microservice.puzzle.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(io.vertx.blueprint.microservice.puzzle.PuzzleService.class)
public class PuzzleService {

  public static final io.vertx.lang.rxjava.TypeArg<PuzzleService> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new PuzzleService((io.vertx.blueprint.microservice.puzzle.PuzzleService) obj),
    PuzzleService::getDelegate
  );

  private final io.vertx.blueprint.microservice.puzzle.PuzzleService delegate;
  
  public PuzzleService(io.vertx.blueprint.microservice.puzzle.PuzzleService delegate) {
    this.delegate = delegate;
  }

  public io.vertx.blueprint.microservice.puzzle.PuzzleService getDelegate() {
    return delegate;
  }

  public PuzzleService retrieveTiles(Handler<AsyncResult<JsonArray>> resultHandlerNonEmpty) { 
    delegate.retrieveTiles(resultHandlerNonEmpty);
    return this;
  }

  public Single<JsonArray> rxRetrieveTiles() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveTiles(fut);
    }));
  }

  public PuzzleService retrieveRows(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) { 
    delegate.retrieveRows(resultHandlerNonEmpty);
    return this;
  }

  public Single<Integer> rxRetrieveRows() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveRows(fut);
    }));
  }

  public PuzzleService retrieveColumns(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) { 
    delegate.retrieveColumns(resultHandlerNonEmpty);
    return this;
  }

  public Single<Integer> rxRetrieveColumns() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveColumns(fut);
    }));
  }

  public PuzzleService retrieveImage(Handler<AsyncResult<String>> resultHandlerNonEmpty) { 
    delegate.retrieveImage(resultHandlerNonEmpty);
    return this;
  }

  public Single<String> rxRetrieveImage() { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveImage(fut);
    }));
  }

  public PuzzleService selectTile(String position, String id, Handler<AsyncResult<Void>> resultHandler) { 
    delegate.selectTile(position, id, resultHandler);
    return this;
  }

  public Single<Void> rxSelectTile(String position, String id) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      selectTile(position, id, fut);
    }));
  }

  public PuzzleService deselectTile(String id, Handler<AsyncResult<Void>> resultHandler) { 
    delegate.deselectTile(id, resultHandler);
    return this;
  }

  public Single<Void> rxDeselectTile(String id) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      deselectTile(id, fut);
    }));
  }

  public PuzzleService retrieveLastColor(String position, Handler<AsyncResult<String>> resultHandlerNonEmpty) { 
    delegate.retrieveLastColor(position, resultHandlerNonEmpty);
    return this;
  }

  public Single<String> rxRetrieveLastColor(String position) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      retrieveLastColor(position, fut);
    }));
  }


  public static PuzzleService newInstance(io.vertx.blueprint.microservice.puzzle.PuzzleService arg) {
    return arg != null ? new PuzzleService(arg) : null;
  }
}
