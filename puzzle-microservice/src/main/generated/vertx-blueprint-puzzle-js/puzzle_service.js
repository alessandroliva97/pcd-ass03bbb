/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module vertx-blueprint-puzzle-js/puzzle_service */
var utils = require('vertx-js/util/utils');

var io = Packages.io;
var JsonObject = io.vertx.core.json.JsonObject;
var JPuzzleService = Java.type('io.vertx.blueprint.microservice.puzzle.PuzzleService');

/**
 @class
*/
var PuzzleService = function(j_val) {

  var j_puzzleService = j_val;
  var that = this;

  /**

   @public
   @param resultHandlerNonEmpty {function} 
   @return {PuzzleService}
   */
  this.retrieveTiles = function(resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_puzzleService["retrieveTiles(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(utils.convReturnJson(ar.result()), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param resultHandlerNonEmpty {function} 
   @return {PuzzleService}
   */
  this.retrieveRows = function(resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_puzzleService["retrieveRows(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param resultHandlerNonEmpty {function} 
   @return {PuzzleService}
   */
  this.retrieveColumns = function(resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_puzzleService["retrieveColumns(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param resultHandlerNonEmpty {function} 
   @return {PuzzleService}
   */
  this.retrieveImage = function(resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 1 && typeof __args[0] === 'function') {
      j_puzzleService["retrieveImage(io.vertx.core.Handler)"](function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param position {string} 
   @param id {string} 
   @param resultHandler {function} 
   @return {PuzzleService}
   */
  this.selectTile = function(position, id, resultHandler) {
    var __args = arguments;
    if (__args.length === 3 && typeof __args[0] === 'string' && typeof __args[1] === 'string' && typeof __args[2] === 'function') {
      j_puzzleService["selectTile(java.lang.String,java.lang.String,io.vertx.core.Handler)"](position, id, function(ar) {
      if (ar.succeeded()) {
        resultHandler(null, null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param id {string} 
   @param resultHandler {function} 
   @return {PuzzleService}
   */
  this.deselectTile = function(id, resultHandler) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_puzzleService["deselectTile(java.lang.String,io.vertx.core.Handler)"](id, function(ar) {
      if (ar.succeeded()) {
        resultHandler(null, null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param position {string} 
   @param resultHandlerNonEmpty {function} 
   @return {PuzzleService}
   */
  this.retrieveLastColor = function(position, resultHandlerNonEmpty) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_puzzleService["retrieveLastColor(java.lang.String,io.vertx.core.Handler)"](position, function(ar) {
      if (ar.succeeded()) {
        resultHandlerNonEmpty(ar.result(), null);
      } else {
        resultHandlerNonEmpty(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  // A reference to the underlying Java delegate
  // NOTE! This is an internal API and must not be used in user code.
  // If you rely on this property your code is likely to break if we change it / remove it without warning.
  this._jdel = j_puzzleService;
};

PuzzleService._jclass = utils.getJavaClass("io.vertx.blueprint.microservice.puzzle.PuzzleService");
PuzzleService._jtype = {
  accept: function(obj) {
    return PuzzleService._jclass.isInstance(obj._jdel);
  },
  wrap: function(jdel) {
    var obj = Object.create(PuzzleService.prototype, {});
    PuzzleService.apply(obj, arguments);
    return obj;
  },
  unwrap: function(obj) {
    return obj._jdel;
  }
};
PuzzleService._create = function(jdel) {
  var obj = Object.create(PuzzleService.prototype, {});
  PuzzleService.apply(obj, arguments);
  return obj;
}
module.exports = PuzzleService;