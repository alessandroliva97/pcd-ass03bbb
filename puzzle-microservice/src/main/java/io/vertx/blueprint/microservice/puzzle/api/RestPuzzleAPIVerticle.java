package io.vertx.blueprint.microservice.puzzle.api;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.HashSet;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.blueprint.microservice.puzzle.PuzzleService;
import io.vertx.blueprint.microservice.puzzle.Tile;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

public class RestPuzzleAPIVerticle extends RestAPIVerticle {

	public static final String SERVICE_NAME = "puzzle-rest-api";

	private static final String API_TILES = "/tiles";
	private static final String API_SELECT = "/select/:position/:id";
	private static final String API_DESELECT = "/deselect/:id";
	private static final String API_ROWS = "/rows";
	private static final String API_COLUMNS = "/columns";
	private static final String API_IMAGE = "/image";
	private static final String API_LAST_COLOR = "/lastcolor/:position";

	private final PuzzleService service;

	public RestPuzzleAPIVerticle(PuzzleService service) {
		this.service = service;
	}

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		final Router router = Router.router(vertx);
		// CORS support
		enableCorsSupport(router);
		// body handler
		router.route().handler(BodyHandler.create());
		// API route handler
		router.get(API_TILES).handler(this::apiRetrieveTiles);
		router.get(API_ROWS).handler(this::apiRetrieveRows);
		router.get(API_COLUMNS).handler(this::apiRetrieveColumns);
		router.get(API_IMAGE).handler(this::apiRetrieveImage);
		router.post(API_SELECT).handler(this::apiSelectTile);
		router.post(API_DESELECT).handler(this::apiDeselectTile);
		router.get(API_LAST_COLOR).handler(this::apiRetrieveLastColor);

		// get HTTP host and port from configuration, or use default value
		String host = config().getString("puzzle.http.address", "0.0.0.0");
		int port = config().getInteger("puzzle.http.port", 8082);

		// create HTTP server and publish REST service
		createHttpServer(router, host, port).compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port))
				.setHandler(future.completer());
	}

	private void apiRetrieveTiles(RoutingContext context) {
		service.retrieveTiles(resultHandler(context, r -> {
			String result = new JsonObject().put("tiles", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*").end(result);
		}));
	}

	private void apiRetrieveRows(RoutingContext context) {
		service.retrieveRows(resultHandler(context, r -> {
			String result = new JsonObject().put("rows", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*").end(result);
		}));
	}

	private void apiRetrieveColumns(RoutingContext context) {
		service.retrieveColumns(resultHandler(context, r -> {
			String result = new JsonObject().put("columns", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*").end(result);
		}));
	}

	private void apiRetrieveImage(RoutingContext context) {
		service.retrieveImage(resultHandler(context, r -> {
			String result = new JsonObject().put("image", r).encodePrettily();
			context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*").end(result);
		}));
	}

	private void apiSelectTile(RoutingContext context) {
		try {
			String position = context.request().getParam("position");
			String id = context.request().getParam("id");
			service.selectTile(position, id, resultHandler(context, r -> {
				String result = new JsonObject().put("message", "tile selected").put("position", position).put("id", id)
						.encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*").end(result);
				vertx.eventBus().publish("update", r);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}
	
	private void apiDeselectTile(RoutingContext context) {
		try {
			String id = context.request().getParam("id");
			service.deselectTile(id, resultHandler(context, r -> {
				String result = new JsonObject().put("message", "tile deselected").put("id", id)
						.encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json").end(result);
				vertx.eventBus().publish("update", r);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}

	private void apiRetrieveLastColor(RoutingContext context) {
		try {
			String position = context.request().getParam("position");
			service.retrieveLastColor(position, resultHandler(context, r -> {
				String result = new JsonObject().put("color", r).encodePrettily();
				context.response().setStatusCode(201).putHeader("Content-Type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*").end(result);
			}));
		} catch (DecodeException e) {
			badRequest(context, e);
		}
	}

}
