package io.vertx.blueprint.microservice.puzzle;

import static io.vertx.blueprint.microservice.puzzle.PuzzleService.SERVICE_ADDRESS;

import io.vertx.blueprint.microservice.common.BaseMicroserviceVerticle;
import io.vertx.blueprint.microservice.puzzle.api.RestPuzzleAPIVerticle;
import io.vertx.blueprint.microservice.puzzle.impl.PuzzleServiceImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ProxyHelper;

public class PuzzleVerticle extends BaseMicroserviceVerticle {

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();

		// create the service instance
		PuzzleService puzzleService = new PuzzleServiceImpl(discovery);
		// register the service proxy on event bus
		ProxyHelper.registerService(PuzzleService.class, vertx, puzzleService, SERVICE_ADDRESS);
		// publish the service in the discovery infrastructure
		publishEventBusService(PuzzleService.SERVICE_NAME, SERVICE_ADDRESS, PuzzleService.class)
				.compose(servicePublished -> deployRestService(puzzleService)).setHandler(future.completer());
	}

	private Future<Void> deployRestService(PuzzleService service) {
		Future<String> future = Future.future();
		vertx.deployVerticle(new RestPuzzleAPIVerticle(service), new DeploymentOptions().setConfig(config()),
				future.completer());
		return future.map(r -> null);
	}

}
