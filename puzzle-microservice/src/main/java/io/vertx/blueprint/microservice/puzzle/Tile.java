package io.vertx.blueprint.microservice.puzzle;

import java.io.Serializable;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject
public class Tile implements Serializable, Comparable<Tile> {

	private static final long serialVersionUID = -804739811900045741L;
	private final int originalPosition;
	private int currentPosition;

	public Tile(final JsonObject json) {
		this.originalPosition = json.getInteger("originalPosition");
		this.currentPosition = json.getInteger("currentPosition");
	}

	public Tile(final int originalPosition, final int currentPosition) {
		this.originalPosition = originalPosition;
		this.currentPosition = currentPosition;
	}

	public boolean isInRightPlace() {
		return currentPosition == originalPosition;
	}

	public int getOriginalPosition() {
		return originalPosition;
	}

	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(final int newPosition) {
		currentPosition = newPosition;
	}

	@Override
	public String toString() {
		return ((Integer) originalPosition).toString();
	}

	@Override
	public int hashCode() {
		return ((Integer) originalPosition).hashCode();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Tile && originalPosition == ((Tile) o).getOriginalPosition();
	}

	@Override
	public int compareTo(Tile other) {
		return this.currentPosition < other.currentPosition ? -1
				: this.currentPosition == other.currentPosition ? 0 : 1;
	}
}
