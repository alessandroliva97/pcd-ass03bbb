/**
 * Indicates that this module contains classes that need to be generated /
 * processed.
 */
@ModuleGen(name = "vertx-blueprint-puzzle", groupPackage = "io.vertx.blueprint.microservice.puzzle")

package io.vertx.blueprint.microservice.puzzle;

import io.vertx.codegen.annotations.ModuleGen;