package io.vertx.blueprint.microservice.puzzle.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import io.vertx.blueprint.microservice.player.PlayerService;
import io.vertx.blueprint.microservice.puzzle.PuzzleService;
import io.vertx.blueprint.microservice.puzzle.Tile;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;

public class PuzzleServiceImpl implements PuzzleService {

	private final int rows = 3;
	private final int columns = 5;
	private final String imagePath = "bletchley-park-mansion.jpg";
	private final List<Tile> tiles = new ArrayList<>();
	private final Map<Integer, List<Integer>> selections = new HashMap<>();

	private final ServiceDiscovery discovery;

	public PuzzleServiceImpl(ServiceDiscovery discovery) {
		this.discovery = discovery;
		final List<Integer> randomPositions = new ArrayList<>();
		IntStream.range(0, rows * columns).forEach(item -> randomPositions.add(item));
		Collections.shuffle(randomPositions);
		IntStream.range(0, rows * columns).forEach(item -> tiles.add(new Tile(item, randomPositions.get(item))));
		Collections.sort(tiles);
		IntStream.range(0, rows * columns).forEach(item -> selections.put(item, new ArrayList<>()));
	}

	private void swapTiles(String position1, String position2) {
		int p1 = Integer.parseInt(position1);
		int p2 = Integer.parseInt(position2);
		tiles.get(p1).setCurrentPosition(p2);
		tiles.get(p2).setCurrentPosition(p1);
		Collections.sort(tiles);
	}

	@Override
	public PuzzleService retrieveTiles(Handler<AsyncResult<JsonArray>> resultHandlerNonEmpty) {
		JsonArray arr = new JsonArray();
		tiles.forEach(e -> arr.add(new JsonObject().put("currentPosition", e.getCurrentPosition())
				.put("originalPosition", e.getOriginalPosition())));
		resultHandlerNonEmpty.handle(Future.succeededFuture(arr));
		return this;
	}

	@Override
	public PuzzleService retrieveRows(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) {
		resultHandlerNonEmpty.handle(Future.succeededFuture(rows));
		return this;
	}

	@Override
	public PuzzleService retrieveColumns(Handler<AsyncResult<Integer>> resultHandlerNonEmpty) {
		resultHandlerNonEmpty.handle(Future.succeededFuture(columns));
		return this;
	}

	@Override
	public PuzzleService retrieveImage(Handler<AsyncResult<String>> resultHandlerNonEmpty) {
		resultHandlerNonEmpty.handle(Future.succeededFuture(imagePath));
		return this;
	}

	@Override
	public PuzzleService selectTile(String position, String id, Handler<AsyncResult<Void>> resultHandler) {
		Optional<Map.Entry<Integer, List<Integer>>> playerSelection = selections.entrySet().stream()
				.filter(e -> e.getValue().contains(Integer.parseInt(id))).findFirst();
		if (playerSelection.isPresent()) {
			selections.get(playerSelection.get().getKey()).remove((Integer) Integer.parseInt(id));
			swapTiles(playerSelection.get().getKey().toString(), position);
		} else {
			selections.get(Integer.parseInt(position)).add(Integer.parseInt(id));
		}
		resultHandler.handle(Future.succeededFuture());
		return this;
	}

	@Override
	public PuzzleService deselectTile(String id, Handler<AsyncResult<Void>> resultHandler) {
		selections.entrySet().stream().filter(e -> e.getValue().contains(Integer.parseInt(id))).findFirst()
			.ifPresent(s -> selections.get(s.getKey()).remove((Integer) Integer.parseInt(id)));
		resultHandler.handle(Future.succeededFuture());
		return this;
	}

	@Override
	public PuzzleService retrieveLastColor(String position, Handler<AsyncResult<String>> resultHandlerNonEmpty) {
		final Color defaultColor = Color.GRAY;
		int pos = Integer.parseInt(position);
		if (selections.isEmpty()) {
			resultHandlerNonEmpty.handle(Future.succeededFuture(String.format("#%02x%02x%02x", defaultColor.getRed(),
					defaultColor.getGreen(), defaultColor.getBlue())));
		} else {
			if (!selections.get(pos).isEmpty()) {
				final int lastId = selections.get(pos).get(selections.get(pos).size() - 1);
				EventBusService.getProxy(discovery, PlayerService.class, ar -> {
					if (ar.succeeded()) {
						PlayerService service = ar.result();
						Future<JsonArray> future = Future.future();
						service.retrieveClients(future.completer());
						Map<Integer, Color> clients = new HashMap<>();
						future.setHandler(x -> {
							future.result().forEach(e -> {
								JsonObject obj = (JsonObject) e;
								clients.put(obj.getInteger("tile"), Color.decode(obj.getString("lastColor")));
							});
							if (clients.containsKey(lastId)) {
								final Color color = clients.get(lastId);
								resultHandlerNonEmpty.handle(Future.succeededFuture(String.format("#%02x%02x%02x",
										color.getRed(), color.getGreen(), color.getBlue())));
							} else {
								resultHandlerNonEmpty.handle(Future.succeededFuture(String.format("#%02x%02x%02x",
										defaultColor.getRed(), defaultColor.getGreen(), defaultColor.getBlue())));
							}
						});
					} else {
						resultHandlerNonEmpty.handle(Future.failedFuture("Required auxiliary service is offline"));
					}
				});
			} else {
				resultHandlerNonEmpty.handle(Future.succeededFuture(String.format("#%02x%02x%02x",
						defaultColor.getRed(), defaultColor.getGreen(), defaultColor.getBlue())));
			}
		}
		return this;
	}

}
