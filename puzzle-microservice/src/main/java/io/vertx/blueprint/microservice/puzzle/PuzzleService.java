package io.vertx.blueprint.microservice.puzzle;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;

import java.util.List;
import java.util.Map;

@VertxGen
@ProxyGen
public interface PuzzleService {

	/**
	 * The name of the event bus service.
	 */
	String SERVICE_NAME = "puzzle-eb-service";

	/**
	 * The address on which the service is published.
	 */
	String SERVICE_ADDRESS = "service.puzzle";

	@Fluent
	PuzzleService retrieveTiles(Handler<AsyncResult<JsonArray>> resultHandlerNonEmpty);

	@Fluent
	PuzzleService retrieveRows(Handler<AsyncResult<Integer>> resultHandlerNonEmpty);

	@Fluent
	PuzzleService retrieveColumns(Handler<AsyncResult<Integer>> resultHandlerNonEmpty);

	@Fluent
	PuzzleService retrieveImage(Handler<AsyncResult<String>> resultHandlerNonEmpty);

	@Fluent
	PuzzleService selectTile(String position, String id, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	PuzzleService deselectTile(String id, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	PuzzleService retrieveLastColor(String position, Handler<AsyncResult<String>> resultHandlerNonEmpty);

}
