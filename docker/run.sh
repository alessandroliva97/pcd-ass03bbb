#!/usr/bin/env bash

set -e

# TODO: set env for docker-machine in Windows and OSX


# export docker-machine IP
IP=127.0.0.1
unamestr=`uname`
if [[ "$unamestr" != 'Linux' ]]; then
  # Set docker-machine IP
  IP="$(docker-machine ip)"
fi
export EXTERNAL_IP=$IP

# Stop
docker-compose -f docker-compose.yml stop

# Start container cluster
# First start persistence and auth container and wait for it
docker-compose -f docker-compose.yml up --force-recreate