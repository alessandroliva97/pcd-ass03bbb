#!/usr/bin/env bash

docker build --no-cache --pull -t "pcd-ass03b/api-gateway" ../api-gateway
docker build --no-cache --pull -t "pcd-ass03b/puzzle-microservice" ../puzzle-microservice
docker build --no-cache --pull -t "pcd-ass03b/player-microservice" ../player-microservice